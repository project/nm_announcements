<?php
/**
 * @file
 * Code for the nm_announcements feature.
 */

include_once 'nm_announcements.features.inc';


/**
 * Implements hook_preprocess_page().
 */
function nm_announcements_preprocess_page(&$variables) {

  //if this is a announcements node
  if (isset($variables['breadcrumb_array']) && isset($variables['node']) && $variables['node']->type == 'nm_announcement') {

    //set array of replacements
    $replacements = array(
      '01' => 'January',
      '02' => 'February',
      '03' => 'March',
      '04' => 'April',
      '05' => 'May',
      '06' => 'June',
      '07' => 'July',
      '08' => 'August',
      '09' => 'September',
      '10' => 'October',
      '11' => 'November',
      '12' => 'December',
    );

    //make the month prettier
    $variables['breadcrumb_array'][3]['title'] = $replacements[$variables['breadcrumb_array'][3]['title']];
    $variables['breadcrumb_array'][3]['hover'] =  $variables['breadcrumb_array'][3]['title'] . ' ' . $variables['breadcrumb_array'][2]['title']; 

    //make new breadcrumbs
    $new_crumbs = nm_breadcrumb_make_crumbs($variables['breadcrumb_array']);

    //set new breacdrumbs
    drupal_set_breadcrumb($new_crumbs);
    $variables['breadcrumb'] = theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb()));

  }  //end if announcement node
}


/**
 * Implements hook_help().
 */
function nm_announcements_help($path, $arg) {
  switch ($path) {
    case 'admin/help#nm_announcements':
      $content = array(
        '#group' => 'info',
        '#theme' => 'item_list',
        '#attributes' => array(
          'class' => array('nodemaker-apps-details'),
        ),
        '#items' => nm_announcements_help_details(),
      );

      return render($content);
      break;
  }
}


/**
  * Content for hook_help().
  */
function nm_announcements_help_details() {
  $items = array();
  $sub1[] = '<strong>' . t('Body Field.') . '</strong>  ' . t('The main content area.');
  $sub1[] = '<strong>' . t('Tags.') . '</strong>  ' . t('For adding multiple keywords as extra categorization of content.');
  $sub1[] = '<strong>' . t('Headline Image.') . '</strong>  ' . t('Added interest for your content.  Displays in teasers and full display.');
  $sub1[] = '<strong>' . t('Related Content.') . '</strong>  ' . t('Manually mark other content on your site as related for intra-site linking.  A block will automatically display with the content.');
  $sub1[] = '<strong>' . t('Attached Gallery.') . '</strong>  ' . t('Display an existing image gallery with your content.  (Note that NodeMaker Galleries must be enabled as well.)');
  $sub1[] = '<strong>' . t('Category.') . '</strong>  ' . t('Categorize your content from a fixed list.  You may !catlink.  If you make changes, you may want to !menulink so they match the current categories.', array('!catlink' => l('edit the list categories', 'admin/structure/taxonomy/nm_announcement_category'), '!menulink' => l('edit the associated menu items', 'admin/structure/menu/manage/main-menu')));
  $items[] = '<strong>' . t('Announcement Content Type.') . '</strong>  ' . t('!add.', array('!add' => l('Create an announcement', 'node/add/nm-announcement'))) . theme('item_list', array('items' => $sub1));
  $items[] = '<strong>' . t('Announcement Category.') . '</strong>  ' . t('You may !catlink.', array('!catlink' => l('edit the list categories', 'admin/structure/taxonomy/nm_announcement_category')));
  $sub2[] = '<strong>' . t('Landing Page with RSS Feed.') . '</strong>  ' . t('A listing of all announcements in reverse chronological order.  An RSS feed of same content.');
  $sub2_1[] = '<strong>' . t('Pages for Year & Month.') . '</strong>  ' . t('All announcements from the specified Year and/or month displayed in reverse chronological order.'); 
  $sub2_1[] = '<strong>' . t('Blocks for Year & Month.') . '</strong>  ' . t('Navigation to archive pages displayed as blocks on all announcement pages allows users to find content by year and month.'); 
  $sub2[] = '<strong>' . t('Date Archive.') . '</strong>  ' . theme('item_list', array('items' => $sub2_1));
  $sub2_2[] = '<strong>' . t('Pages for Each Category.') . '</strong>  ' . t('Each announcement category has a page displaying all announcements in that category in reverse chronology.');
  $sub2_2[] = '<strong>' . t('Block of Categories.') . '</strong>  ' . t('Navigate to any announcement category page from blocks displayed on all announcement pages.');
  $sub2[] = '<strong>' . t('Browse by Category.') . '</strong>  ' . theme('item_list', array('items' => $sub2_2));
  $items[] = '<strong>' . t('Announcement Pages.') . '</strong>  ' . t('') . theme('item_list', array('items' => $sub2));
  $items[] = '<strong>' . t('Menu Item.') . '</strong>  ' . t('There is a link in the main menu to the !landing.  There are nested sub items for each announcement category page.  You may !menu.', array('!landing' => l('announcement landing page', 'announcements'), '!menu' => l('alter the menu items', 'admin/structure/menu/manage/main-menu')));
  $items[] = '<strong>' . t('Breadcrumbs.') . '</strong>  ' . t('All announcement pages have meaningful, usable, accurate breadcrumbs.  (Note that NodeMaker Breadcrumbs must be enabled as well.)');
  $items[] = '<strong>' . t('Issues?') . '</strong>  ' . t('Have a support question?  Find a bug?  !issue!', array('!issue' => '<a href="http://drupal.org/project/issues/nm_announcements" title="NodeMaker Announcements Issue Queue" target="_blank">Please use the issue queue</a>'));
  return $items;
}


function nodemaker_nm_announcements_getting_started_form() {
  $form = nodemaker_nm_announcements_getting_started_content();
  return system_settings_form($form);
}


/**
  * Content for nodemaker_getting_started_form().
  * Content for nodemaker_nm_announcements_getting_started_form().
  */
function nodemaker_nm_announcements_getting_started_content() {

  //variables used throughout form
  $destination = drupal_get_destination();
  $destination = $destination['destination'];

  //get the icon
  $icon = drupal_get_path('module', 'nm_announcements') . '/' . 'nm_announcements' . '-icon.png';
  if (file_exists($icon)) {
    $image = l(theme('image', array('path' => $icon, 'attributes' => array('class' => array('nodemaker-icon')))), 'admin/nodemaker/apps/'. 'nm_announcements', array('html' => TRUE, 'attributes' => array('class' => array('nodemaker-icon-link'), 'title' => 'NodeMaker Announcements Details')));
  }
  else {
    $image = '';
  }

  //group
  $form['tabs']['nm_announcements_getting_started'] = array(
    '#type' => 'fieldset',
    '#title' => t('NodeMaker Announcements'),
    '#description' => $image . t('<h4>NodeMaker Announcements Launch Checklist</h4><p>NodeMaker Announcements provides many default configurations, including several that were defined during installation. This launch checklist will help you understand some of the configurations that you should verify are correct for your needs prior to launching your new website.</p>'),
    '#attributes' => array(
      'class' => array('getting-started'),
    ),
    '#weight' => -10,
  );

  //taxonomy
  $description = t('When you installed NodeMaker Announcements, we created arbitrary announcement categories for you.  Please !cats to match what you need.', array('!cats' => l('edit the list of categories', 'admin/structure/taxonomy/nm_announcement_category', array('query' => array('destination' => $destination)))));
  $form['tabs']['nm_announcements_getting_started']['nm_announcements_getting_started_category'] = array(
    '#type' => 'checkbox',
    '#title' => t('Announcement Categories'),
    '#description' => $description,
    '#default_value' => variable_get('nm_announcements_getting_started_category', 0),
    '#title_display' => 'after',
    '#theme' => 'toggleswitch',
  );

  //menu items
  $description = t('When you installed NodeMaker Announcements, we created nested menu items for each Announcement Category.  If you changed the categories, you may need to !menu.', array('!menu' => l('update the menu', 'admin/structure/menu/manage/main-menu', array('query' => array('destination' => $destination)))));
  $form['tabs']['nm_announcements_getting_started']['nm_announcements_getting_started_menu'] = array(
    '#type' => 'checkbox',
    '#title' => t('Menu Items'),
    '#description' => $description,
    '#default_value' => variable_get('nm_announcements_getting_started_menu', 0),
    '#title_display' => 'after',
    '#theme' => 'toggleswitch',
  );

  return $form;

}